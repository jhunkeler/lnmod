import unittest
import os
import shutil
import tempfile
import lnmod
import logging
import sys


BASEDIR = os.path.abspath('base')
PATTERN_SEARCH = ""
PATTERN_REPLACE = ""


def init_test_data():
    global BASEDIR
    global PATTERN_SEARCH
    global PATTERN_REPLACE
    
    if os.path.exists(BASEDIR):
        deinit_test_data()
    os.mkdir(BASEDIR)
    
    origpath = tempfile.mkdtemp('data', 'lnmod', BASEDIR)
    origpath_file = os.path.join(origpath, 'A')
    
    newpath = tempfile.mkdtemp('data', 'lnmod', BASEDIR)
    newpath_file = os.path.join(newpath, 'B')
    
    PATTERN_SEARCH = origpath
    PATTERN_REPLACE = newpath

    open(origpath_file, 'w+').write('ABCDEFG!')
    os.symlink(origpath_file, newpath_file)
    
    data = (origpath, newpath)
    return data
        
def deinit_test_data():
    shutil.rmtree(BASEDIR)
    
class TestLnmod(unittest.TestCase):
    def test_data(self):
        path_original, path_new = init_test_data()
        self.assertTrue(os.path.exists(BASEDIR))
        self.assertGreater(len(os.listdir(BASEDIR)), 0, "Test data generation failed.")
        self.assertGreater(len(os.listdir(path_original)), 0, "Test data generation failed.")
        self.assertGreater(len(os.listdir(path_new)), 0, "Test data generation failed.")
        deinit_test_data()
    
    def test_generate_link_map_namedtuple(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        for link in lmap:
            self.assertIsInstance(link, lnmod.Link, "Link map did not return list of namedtuples")
        deinit_test_data()

    def test_generate_link_map_validate_paths(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        for link in lmap:
            self.assertTrue(os.path.exists(link.path))
        deinit_test_data()

    def test_generate_link_map_validate_linkage(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        for link in lmap:
            self.assertTrue(os.path.exists(link.destination))
        deinit_test_data()
        
    def test_generate_replacement_link_map_namedtuple(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        rmap = lnmod.generate_replacement_link_map(lmap, PATTERN_SEARCH, PATTERN_REPLACE)
        for link in rmap:
            self.assertIsInstance(link, lnmod.Link, "Replacement link map did not return list of namedtuples")
        deinit_test_data()
        
    def test_generate_replacement_link_map_validate_linkage(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        #self.fail(lmap)
        rmap = lnmod.generate_replacement_link_map(lmap, PATTERN_SEARCH, PATTERN_REPLACE)
        self.assertGreater(len(lmap), 0, "Empty link map! {0}".format(lmap))
        self.assertGreater(len(rmap), 0, "Empty replacement link map! {0}".format(rmap))
        for link in rmap:
            self.assertTrue(os.path.exists(link.path), msg='Original link path disappeared')
        deinit_test_data()
        
    def test_replace_links(self):
        init_test_data()
        lmap = lnmod.generate_link_map(BASEDIR)
        rmap = lnmod.generate_replacement_link_map(lmap, PATTERN_SEARCH, PATTERN_REPLACE)
        try:
            lnmod.replace_links(rmap)
        except OSError as e:
            self.fail('Could not replace link. {0}'.format(e.strerror()))
        except IOError as e:
            self.fail('Could not replace link. {0}'.format(e.strerror()))
            
        deinit_test_data()
if __name__ == "__main__":
    unittest.main()

